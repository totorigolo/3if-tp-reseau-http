/**
 * Enumération représentant les méthodes HTTP.
 */
enum RequestMethod {
    NONE, GET, POST, HEAD, OPTIONS, PUT, DELETE, TRACE, CONNECT, PATCH;

    /**
     * Convertit un String en RequestMethod
     * @param word La supposée méthode HTTP
     * @return La RequestMethod correspondant, ou null s'il n'y en a pas.
     */
    public static RequestMethod getRequestMethod(String word) {
        switch (word.toUpperCase()) {
            case "GET":
                return GET;
            case "POST":
                return POST;
            case "HEAD":
                return HEAD;
            case "OPTIONS":
                return OPTIONS;
            case "PUT":
                return PUT;
            case "DELETE":
                return DELETE;
            case "TRACE":
                return TRACE;
            case "CONNECT":
                return CONNECT;
            case "PATCH":
                return CONNECT;
        }
        return null;
    }

    /**
     * Indique si rm peut avoir un crops de requête.
     * @param rm La RequestMethod à vérifier.
     * @return true si elle peut avoir un corps de requête (ex: POST), sinon false
     */
    public static boolean hasBody(RequestMethod rm) {
        if (rm == null) return false;
        switch (rm) {
            case POST:
            case PUT:
            case CONNECT:
            case PATCH:
                return true;
        }
        return false;
    }
}
