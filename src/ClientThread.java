import java.net.Socket;

/**
 * Chaque requête entrante sur le serveur est exécutée dans un Thread. Cette classe représente ce thread.
 */
class ClientThread extends Thread {

    private Socket clientSocket;

    /**
     * Constructeur de ClientThread
     * @param clientSocket la socket du client
     */
    ClientThread(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    /**
     * Gère la requête du client
     **/
    @Override
    public void run() {
        // Handle the request
        new RequestHandler(clientSocket).process();
    }
}
