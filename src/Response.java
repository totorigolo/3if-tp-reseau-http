import java.io.*;
import java.nio.file.Files;

/**
 * Représente une réponse HTTP.
 */
class Response {

    ResponseHeader header;
    private boolean finalisedHeader;
    private boolean headerSent;
    private PrintStream socOut;
    private ByteArrayOutputStream memoryOutputStream;

    /**
     * Construit la réponse à partir du header de la requête.
     * @param requestHeader Header de la requête
     * @param socOut La socket vers le client.
     */
    Response(RequestHeader requestHeader, PrintStream socOut) {
        this.header = new ResponseHeader(requestHeader);
        this.finalisedHeader = false;
        this.socOut = socOut;
        this.memoryOutputStream = new ByteArrayOutputStream();
    }

    /**
     * Indique que le header est définitif. Cela permet d'envoyer directement sur la socket et non
     * plus en mémoire. Flush en même temps.
     */
    void finaliseHeader() throws IOException {
        finalisedHeader = true;
        flush();
    }

    /**
     * Si le header est finalisé, transmet les données de la requête disponible sur la socket.
     */
    private void flush() throws IOException {
        if (finalisedHeader) {
            if (!headerSent) {
                socOut.print(header.toString());
                socOut.print("\r\n");
                headerSent = true;
            }

            if (memoryOutputStream.size() > 0) {
                memoryOutputStream.writeTo(socOut);
                memoryOutputStream.flush();
            }
        }
    }

    /**
     * Envoie (fini d'envoyer) la requête au client, puis ferme la sortie vers la socket.
     */
    void send() throws IOException {
        finaliseHeader();
        memoryOutputStream.close();
        socOut.close();
    }

    /**
     * Envoie le fichier filename au client, puis ferle la sortie vers la socket.
     * @param filename le nom du fichier à transmettre
     * @return true si tout s'est déroulé sans soucis, sinon false (erreur 404)
     *
     * Voir aussi {see #appendFile}
     */
    boolean sendFile(String filename) throws IOException {
        try {
            File inputFile = new File(filename);
            if (inputFile.exists() && !inputFile.isDirectory()) {
                header.addField("Content-Length:", String.valueOf(inputFile.length()));
                finaliseHeader();
            }
            Files.copy(inputFile.toPath(), getOutputStream());
        } catch (IOException e) {
            this.header.statusCode.setCode(404);
            try {
                File inputFile = new File(Config.webroot + "web_content/404.html");
                Files.copy(inputFile.toPath(), memoryOutputStream);
                this.header.setContentType("text/html");
            } catch (IOException e1) {
                // La page n'existe pas, donc il n'y aura pas de corps
            }
            send();
            return false;
        }
        memoryOutputStream.close();
        socOut.close();
        return true;
    }

    /**
     * Ajoute le fichier filename au corps de la réponse. Contrairement à sendFile(), ne ferme pas
     * la sortie vers la socket.
     *
     * Cette méthode n'est plus utilisée, mais elle pourrait le redevenir si on étend les fonctionnalités
     * du serveur.
     *
     * @param filename le nom du fichier à transmettre
     * @param do404 Indique s'il faut générer une erreur 404 si le fichier est introuvable
     * @return true si tout s'est déroulé sans soucis, sinon false (erreur 404)
     */
    boolean appendFile(String filename, boolean do404) {
        try {
            File inputFile = new File(filename);
            Files.copy(inputFile.toPath(), getOutputStream());
        } catch (IOException e) {
            if (do404) {
                this.header.statusCode.setCode(404);
                try {
                    File inputFile = new File(Config.webroot + "web_content/404.html");
                    Files.copy(inputFile.toPath(), memoryOutputStream);
                    this.header.setContentType("text/html");
                } catch (IOException e1) {
                    // La page n'existe pas, donc il n'y aura pas de corps
                }
                return false;
            }
        }
        return true;
    }

    /**
    /**
     * Permet de récupérer un PrintWriter pour écrire dans le corps de la réponse. Peut-être
     * en mémoire ou en direct sur la socket.
     *
     * ATTENTION :
     *   - ne pas appeler finaliseHeaders() pendant l'utilisation du writer. Il faut alors
     *     close() le writer et refaire un appel à cette méthode.
     *   - ne pas oublier d'appeler close() sur le writer.
     *
     * @return Le PrintWriter pour écrire le corps de la réponse.
     */
    PrintWriter getWriter() {
        return new PrintWriter(getOutputStream());
    }

    private OutputStream getOutputStream() {
        if (finalisedHeader)
            return socOut;
        else
            return memoryOutputStream;
    }
}
