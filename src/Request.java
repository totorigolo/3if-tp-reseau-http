import java.io.UnsupportedEncodingException;
import java.util.HashMap;

/**
 * Représente une requête HTTP
 */
class Request {

    private RequestHeader header;
    private String body;
    private String raw;
    private boolean processingHeader;

    /**
     * Constructeur de Request
     */
    Request() {
        this.header = new RequestHeader();
        this.body = "";
        this.raw = "";
        this.processingHeader = true;
    }

    /**
     * Construit la requête ligne par ligne.
     * @param line la ligne à ajouter à la requête
     * Voir aussi {@link #appendToBody}.
     */
    void processLine(String line) {
        if (line == null) return;

        raw += line + "\r\n";

        // Une ligne vide déclare la fin du header
        if (line.isEmpty()) {
            this.processingHeader = false;
            return;
        }

        if (this.processingHeader) {
            header.processLine(line);
        } else {
            body += String.format("%s\n", line);
        }
    }

    /**
     * Permet de récupérer le header de la requête
     * @return Le header
     */
    RequestHeader getHeader() {
        return header;
    }

    /**
     * Permet de récupérer la requête brute
     * @return Chaîne de caractères représentant la requête
     */
    String getRaw() {
        return raw;
    }

    /**
     * Ajoute le caractère c au corps de la requête.
     * @param c caractère à ajouter. si c inférieur à 0, il n'est pas rajouté
     * @return true si le body est encore incomplet (par rapport à Content-Length), sinon false
     */
    boolean appendToBody(int c) {
        if (c <= 0) return false;
        this.body += (char) c;
        raw += (char) c;
        Integer bodyContentLength = this.header.getContentLength();
        return bodyContentLength != null && this.body.length() < bodyContentLength;
    }

    /**
     * Pour les requêtes POST, renvoi sous forme de HashMap les paramètres.
     * @return la hash map contenant les paramètres
     */
    HashMap<String, String> processBodyPOST() {
        HashMap<String, String> bodyFields = new HashMap<>();
        String fields[] = this.body.split("&");
        for (String field : fields) {
            String key_value[] = field.split("=");
            String value = null;
            if (key_value.length > 1) {
                try {
                    value = java.net.URLDecoder.decode(key_value[1], "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    value = key_value[1];
                }
            }
            bodyFields.put(key_value[0], value);
        }
        return bodyFields;
    }

    /**
     * Indique si cette requête a un corps. La réponse est basée sur la méthode HTTP (ex: GET ne peut pas avoir
     * de corps), et sur Content-Length.
     * @return true si cette a un corps, sinon false
     */
    boolean hasBody() {
        return RequestMethod.hasBody(this.header.getMethod()) && this.header.getContentLength() != null;
    }
}
