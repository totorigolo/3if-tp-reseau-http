import java.net.*;
import java.util.Scanner;

/**
 * Attend les connexions et crée des ClientThread pour les gérer.
 */
class HTTPServer {

    /**
     * Initialise le serveur en fonction de la configuration choisie.
     * @param args Arguments de la ligne de commande
     **/
    public static void main(String args[]) {
        System.out.println("Serveur HTTP démarré.");

        HTTPServer app = new HTTPServer();
        app.run();
    }

    /**
     * Lance l'écoute.
     **/
    private void run() {
        System.out.print("Enter a port to listen to: ");
        Scanner sc = new Scanner(System.in);
        int port = sc.nextInt();
//        int port = 12345;

        try {
            ServerSocket listenSocket;
            listenSocket = new ServerSocket(port);
            System.out.printf("HTTPServer ready at port %d...\n", port);
            while (true) {
                try {
                    Socket clientSocket = listenSocket.accept();
                    System.out.println("Connexion from: " + clientSocket.getInetAddress());

                    new ClientThread(clientSocket).start();
                } catch (Exception e) {
                    System.err.println("Error while processing client request.");
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            System.err.println("Error in HTTPServer: " + e);
        }
    }
}
