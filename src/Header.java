import java.util.HashMap;

/**
 * Représente le header d'un requête ou d'une réponse HTTP.
 */
abstract class Header {

    StatusCode statusCode;
    String HTTPVersion;
    HashMap<String, String> fields;

    /**
     * Constructeur de Header
     * Initialise le status code à 200 (OK).
     */
    Header() {
        statusCode = new StatusCode(200);
        this.fields = new HashMap<>();
    }

    /**
     * Permet d'ajouter un champ à la requête.
     *
     * Pour un champ de la forme :
     *   - Nom-Champ: valeur du champ
     * il faut que :
     *   - key = "Nom-Champ:"
     *   - value = "valeur du champ"
     *
     * @param key Le nom du champ
     * @param value La valeur du champ
     */
    void addField(String key, String value) {
        this.fields.put(key, value);
    }
}
