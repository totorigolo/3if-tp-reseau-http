import java.util.Scanner;

/**
 * Représente un header de requête HTTP.
 */
class RequestHeader extends Header {

    private String url;
    private RequestMethod requestMethod;

    /**
     * Constructeur.
     */
    RequestHeader() {
        this.requestMethod = RequestMethod.NONE;
    }

    /**
     * Construit le header à partir des lignes de la requête.
     * @param line la ligne de la requête à ajouter
     * @return false si la requête est invalide, true sinon
     */
    boolean processLine(String line) {
        if (line == null || line.length() == 0) {
            System.err.println("I shouldn't receive the empty line.");
            return false;
        }

        Scanner scanner = new Scanner(line);

        // Clé
        if (!scanner.hasNext()) {
            statusCode.setCode(400);
            System.err.println("No key.");
            return false;
        }
        String key = scanner.next();
        if (requestMethod == RequestMethod.NONE && (requestMethod = RequestMethod.getRequestMethod(key)) != null) {
            // URL
            if (!scanner.hasNext()) {
                statusCode.setCode(400);
                System.err.printf("No URL specified (line = \"%s\")\n", line);
                return false;
            }
            url = scanner.next();

            // HTTP version
            if (!scanner.hasNext()) {
                statusCode.setCode(400);
                System.err.printf("No HTTP version (line = \"%s\")\n", line);
                return false;
            }
            HTTPVersion = scanner.next();

            System.out.println(this.toString());

            return true;
        }
        key = key.substring(0, key.length() - 1).toLowerCase();

        // Valeur
        scanner.useDelimiter("\\z"); // Jusqu'à la fin de la String
        if (!scanner.hasNext()) {
            statusCode.setCode(400);
            System.err.printf("No value for key <%s>.", key);
            return false;
        }
        String value = scanner.next().substring(1);

        // Enregistre
        this.fields.put(key, value);

        return true;
    }

    /**
     * Permet de récupérer la méthode de la requête.
     * @return La méthode HTTP
     */
    RequestMethod getMethod() {
        return requestMethod;
    }

    /**
     * Permet de récupérer le code de status de la requête.
     * @return La méthode
     */
    StatusCode getStatusCode() {
        return statusCode;
    }

    /**
     * Renvoie l'URL demandée dans la requête.
     * @return Une chaîne de caractères représentant l'URL.
     */
    String getUrl() {
        return url;
    }

    /**
     * Converti la requête en String. Indique la versions HTTP, la méthode HTTP ainsi que l'URL demandée.
     * @return La chaîne de caractères représentant ce header.
     */
    @Override
    public String toString() {
        return HTTPVersion + " " + requestMethod + " " + url;
    }

    /**
     * Renvoie la valeur du champ "Content-Length" de la requête. Si ce champ est absent, null est retourné.
     * @return La valeur de Content-Length, ou null si absent
     */
    Integer getContentLength() {
        String cl = this.fields.get("content-length");
        return (cl == null) ? null : Integer.parseInt(cl);
    }
}
