import java.io.*;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Map;

/**
 * Traîte la requête HTTP du client.
 */
class RequestHandler {
    private Request request;
    private Response response;
    private BufferedReader socIn;
    private Socket clientSocket;

    /**
     * Constructeur.
     * @param clientSocket la socket du client sur laquelle communiquer.
     */
    RequestHandler(Socket clientSocket) {
        this.clientSocket = clientSocket;
        this.response = null;
        this.request = new Request();
    }

    /**
     * Gère les entrées / sorties socket, et lance {@link #doInput()} et {@link #doRequest()} en gérant
     * les exceptions.
     */
    void process() {
        PrintStream socOut;
        try {
            this.socIn = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            socOut = new PrintStream(clientSocket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        try {
            doInput();
            this.response = new Response(request.getHeader(), socOut);
            doRequest();
        } catch (Exception e) {
            e.printStackTrace();
            socOut.print("HTTP/1.0 520 Web server is returning an unknown error\r\n");
        }

        // Close the socket
        try {
            socOut.close();
            socIn.close();
        } catch (IOException e) {
            System.err.println("Error when closing client socket.");
            e.printStackTrace();
        }
    }

    /**
     * Construit la requête à partir des données entrantes sur la socket.
     */
    private void doInput() {
        try {
            String line;
            boolean header = true;
            this.clientSocket.setSoTimeout(5 * 1000); // 5 sec
            while (header) {
                line = socIn.readLine();
                request.processLine(line);
                if (line != null && line.isEmpty()) {
                    header = false;
                }
            }

            if (request.hasBody()) {
                while (request.appendToBody(socIn.read())) ;
            }
        } catch (SocketTimeoutException e) {
            // Si on est là, c'est parce qu'il y a eu un timeout du socket
            request.getHeader().statusCode.setCode(408);
        } catch (Exception e) {
            request.getHeader().statusCode.setCode(520);
            e.printStackTrace();
        }
    }

    /**
     * Gère les erreurs de le requête, s'il y en a eu, et exécute la méthode spécifique à la méthode HTTP.
     */
    private void doRequest() throws IOException {
        // If there is an error in the header, handle it
        if (request.getHeader().getStatusCode().getCode() >= 300) {
            response.getWriter().print(request.getRaw());
            response.send();
            return;
        }

        // Redirect to specific functions
        switch (this.request.getHeader().getMethod()) {
            case GET:
                get();
                break;
            case POST:
                post();
                break;
            case HEAD:
                head();
                break;
            case TRACE:
                trace();
                break;
            case OPTIONS:
                options();
                break;
            default:
                this.response.header.statusCode.setCode(501);
        }
    }

    /**
     * Répond aux requêtes GET en renvoyant la ressource demandée.
     */
    private void get() throws IOException {
        String url = request.getHeader().getUrl();
        int indexParams = url.indexOf("?");
        if (indexParams >= 0)
            url = url.substring(0, indexParams);
        if (url.equals("/")) url = "/index.html";
        response.header.setContentTypeFromUrl(url);

        response.sendFile(Config.webroot + "web_content" + url);
    }

    /**
     * Répond aux requêtes POST en envoyant une page HTML exposant les paramètres reçus.
     */
    private void post() throws IOException {
        HashMap<String, String> fields = request.processBodyPOST();

        final int nbFields = fields.size();
        PrintWriter writer = response.getWriter();
        writer.print("<h1>Paramètres contenus dans la requête :</h1>\r\n");
        if (nbFields > 0) {
            writer.print("<p>Il y a " + nbFields + " paramètre" + ((nbFields != 1) ? "s" : "") + " dans la requête POST.</p>\r\n");
            writer.print("<ul>\r\n");
            for (Map.Entry<String, String> entry : fields.entrySet()) {
                writer.print("<li>" + entry.getKey() + " => " + entry.getValue() + "</li>\r\n");
            }
            writer.print("</ul>\r\n");
        } else {
            writer.print("<p>Il n'y a pas de paramètres dans la requête POST.</p>");
        }
        writer.print("<a href=\"" + request.getHeader().getUrl() + "\">Aller à la page cible de la méthode.</a>");
        writer.close();

        response.header.setContentType("text/html; charset=utf-8");
        response.header.statusCode.setCode(200);
        response.send();
    }

    /**
     * Répond aux requêtes HEAD. Ne gère que l'erreur 404.
     */
    private void head() throws IOException {
        String url = request.getHeader().getUrl();
        if (url.equals("/")) url = "/index.html";
        response.header.setContentTypeFromUrl(url);

        if (!new File(Config.webroot + "web_content" + url).isFile()) {
            response.header.statusCode.setCode(404);
            return;
        }

        response.header.statusCode.setCode(200);
        response.send();
    }

    /**
     * Répond aux requêtes TRACE en renvoyant la requête en corps de réponse.
     */
    private void trace() throws IOException {
        PrintWriter writer = response.getWriter();
        writer.print(request.getRaw());
        writer.close();
        response.header.statusCode.setCode(200);
        response.send();
    }

    /**
     * Répond aux requêtes OPTIONS en indiquant les méthodes HTTP disponibles.
     */
    private void options() throws IOException {
        response.header.addField("Allow:", "GET,HEAD,POST,OPTIONS,TRACE");
        response.header.statusCode.setCode(200);
        response.send();
    }
}
