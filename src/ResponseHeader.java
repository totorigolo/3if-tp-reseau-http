import java.text.SimpleDateFormat;
import java.util.Map;

/**
 * Représente un header d'une réponse HTTP.
 */
class ResponseHeader extends Header {

    private String contentType;

    /**
     * Construit le header à partir de celui de la requête. Par défaut, le content type
     * est considérée text/plain.
     * @param requestHeader la requête associée
     */
    ResponseHeader(RequestHeader requestHeader) {
        this.statusCode = requestHeader.getStatusCode();
        this.HTTPVersion = requestHeader.HTTPVersion;
        this.contentType = "text/plain";
    }

    /**
     * Change le content type de la réponse.
     * @param contentType nouveau content type
     */
    void setContentType(String contentType) {
        this.contentType = contentType;
    }

    /**
     * Change le content type de la réponse à partir d'une URL.
     * @param url l'URL. Doit être sans aucun paramètre ni référence HTML.
     */
    void setContentTypeFromUrl(String url) {
        String ext = url.substring(url.lastIndexOf(".") + 1);
        switch (ext.toLowerCase()) {
            case "html":
                this.contentType = "text/html";
                break;
            case "css":
                this.contentType = "text/css";
                break;
            case "xhtml":
                this.contentType = "text/xhtml";
                break;
            case "png":
                this.contentType = "image/png";
                break;
            case "jpg":
            case "jpeg":
                this.contentType = "image/jpeg";
                break;
            case "gif":
                this.contentType = "image/gif";
                break;
            case "mp4":
                this.contentType = "video/mp4";
                break;
            case "txt":
            default:
                this.contentType = "text/plain";
        }
    }

    /**
     * Transforme ce header en String, pour l'envoyer au navigateur
     * @return Le String résultant.
     */
    @Override
    public String toString() {
        /*
HTTP/1.1 200 OK
Date: Mon, 23 May 2005 22:38:34 GMT
Content-Type: text/html; charset=UTF-8
Content-Encoding: UTF-8
Content-Length: 138
Last-Modified: Wed, 08 Jan 2003 23:11:55 GMT
Server: Apache/1.3.3.7 (Unix) (Red-Hat/Linux)
ETag: "3f80f-1b6-3e1cb03b"
Accept-Ranges: bytes
Connection: close
         */
        String s = "HTTP/1.0 " + statusCode + "\r\n" +
                "Date: " + new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z").format(new java.util.Date()) + "\r\n" +
                "Content-Type: " + contentType + "\r\n" +
                "Server: Nyan Server\r\n";
        for (Map.Entry<String, String> entry : fields.entrySet()) {
            s += entry.getKey() + " " + entry.getValue() + "\r\n";
        }
        s += "Connection: close\r\n";
        return s;
    }
}
